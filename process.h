class process {
private:
	int arrival_time;
	int burst_time;
	int premp_time;
	int burst_left;
	int pid;
public:
	process(int burst, int time, int id);
	void set_arrival(int time);
	void set_pid(int num);
	int get_arrival();
	int get_burst();
	int get_burst_left();
	int get_pid();
};

process::process(int burst, int time, int id)
{
	pid = id;
	burst_time = burst;
	burst_left = burst;
	arrival_time = time;
}

void process::set_pid(int num)
{
	pid = num;
}


int process::get_burst()
{
	return burst_time;
}
int process::get_burst_left()
{
	return burst_left;
}

int process::get_pid()
{
	return pid;
}

void process::set_arrival(int time)
{
	arrival_time = time;
}

int process::get_arrival()
{
	return arrival_time;
}

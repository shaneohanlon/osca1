CXX = g++
CXXFLAGS = -std=c++0x -Wall

OBJECTS = simulate.o process.o

main: $(OBJECTS)
        $(CXX) $(CXXFLAGS) -o $@ $^

$(OBJECTS): process.hpp